package com.noser.prototype


data class AddPointRequest(val name: String, val points: Int)

object PointsService {

    val db = PointsDatabase()

    fun getPoints(): List<Player> {
        return db.data.values.toList()
    }

    suspend fun onPointsBlocking(req: AddPointRequest) {
        db.addPointsBlocking(req.name, req.points)
    }

    suspend fun onPointsAsync(req: AddPointRequest) {
        db.addPointsAsync(req.name, req.points)
    }

    suspend fun onPointsAsyncSingle(req: AddPointRequest) {
        db.addPointsAsyncSingleThread(req.name, req.points)
    }

}
