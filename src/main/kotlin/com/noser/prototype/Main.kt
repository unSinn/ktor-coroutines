package com.noser.prototype

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.gson.gson
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode.Companion.InternalServerError
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.put
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import java.text.DateFormat.LONG


fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        install(StatusPages) {
            exception<Throwable> { e ->
                e.printStackTrace()
                call.respondText(e.localizedMessage, ContentType.Text.Plain, InternalServerError)
            }
        }
        install(ContentNegotiation) {
            gson {
                setDateFormat(LONG)
                setPrettyPrinting()
            }
        }
        routing {

            get("/points") {
                call.respond(PointsService.getPoints())
            }

            put("/addblocking") {
                val req = call.receive<AddPointRequest>()
                PointsService.onPointsBlocking(req)
                call.respond(OK)
            }
            put("/addasync") {
                val req = call.receive<AddPointRequest>()
                PointsService.onPointsAsync(req)
                call.respond(OK)
            }
            put("/addasyncsingle") {
                val req = call.receive<AddPointRequest>()
                PointsService.onPointsAsyncSingle(req)
                call.respond(OK)
            }
        }
    }
    server.start(wait = true)
}