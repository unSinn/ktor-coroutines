package com.noser.prototype

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.newSingleThreadContext
import mu.KLogging
import java.lang.Thread.sleep


data class Player(val name: String, val points: Int)

class PointsDatabase {

    companion object : KLogging()

    private val singleThreadDispatcher = newSingleThreadContext("singleThreadDispatcher")

    val data = mutableMapOf<String, Player>()

    fun addPoint(name: String) {
        val player = data.getOrPut(name) { Player(name, 0) }
        data[name] = Player(player.name, player.points + 1)
    }

    suspend fun addPointsBlocking(name: String, points: Int) {
        logger.info { "Started for $name $points" }
        for (i in 1..points) {
            delay(1)
            addPoint(name)
        }
        logger.info { "Done for $name $points" }
    }

    suspend fun addPointsAsync(name: String, points: Int): Deferred<Unit> {
        return async {
            addPointsBlocking(name, points)
        }
    }

    suspend fun addPointsAsyncSingleThread(name: String, points: Int): Deferred<Unit> {
        return async(singleThreadDispatcher) {
            addPointsBlocking(name, points)
        }
    }

}
