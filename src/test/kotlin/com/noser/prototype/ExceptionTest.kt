package com.noser.prototype

import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mu.KLogging
import org.junit.Test


class ExceptionTest {
    companion object : KLogging()


    class TestException(it: String) : Exception("Something went wrong with $it")

    @Test(expected = TestException::class)
    fun catchException() {
        runBlocking {
            val job = async {
                delay(10)
                throw TestException("job")
            }
            job.await()
        }
    }

    @Test
    fun catchAsyncException() {
        runBlocking {
            try {
                val job = async {
                    delay(10)
                    throw TestException("job")
                }
                job.await()
            } catch (e: TestException) {
                logger.info { "Got $e" }
            }
        }
    }

    @Test
    fun catchMultiStepException() {
        runBlocking {
            try {
                val job1 = async {
                    good()
                }
                val job2 = async {
                    bad()
                }
                logger.info { "waiting..." }
                listOf(job1, job2).map { it.await() }
            } catch (e: TestException) {
                logger.info { "Got $e" }
            }
        }
    }

    private suspend fun good() {
        delay(10)
        logger.info { "This went good" }
    }

    private suspend fun bad() {
        delay(20)
        logger.info { "This seems to go bad good" }
        throw TestException("bad")
    }

}
