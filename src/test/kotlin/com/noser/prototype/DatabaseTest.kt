package com.noser.prototype

import assertk.assert
import assertk.assertions.isEqualTo
import kotlinx.coroutines.runBlocking
import org.junit.Test
import java.lang.Thread.sleep
import java.util.concurrent.Executors
import kotlin.concurrent.thread
import kotlin.system.measureTimeMillis


class DatabaseTest {

    @Test
    fun increment() {
        val db = PointsDatabase()
        db.addPoint("Hans")
        db.addPoint("Peter")
        db.addPoint("Hans")
        assert(db.data["Peter"]?.points).isEqualTo(1)
        assert(db.data["Hans"]?.points).isEqualTo(2)
    }

    @Test
    fun incrementPoints() {
        val db = PointsDatabase()
        runBlocking {
            val job = db.addPointsAsync("Hans", 1000)
            job.await()
        }
        assert(db.data["Hans"]?.points).isEqualTo(1000)
    }

    @Test
    fun incrementPointsBlocking() {
        val db = PointsDatabase()
        val ms = measureTimeMillis {
            runBlocking {
                db.addPointsBlocking("Hans", 1000)
                db.addPointsBlocking("Hans", 800)
                db.addPointsBlocking("Hans", 400)
            }
        }
        println("Took $ms ms")
        assert(db.data["Hans"]?.points).isEqualTo(2200)
    }

    @Test
    fun incrementPointsConcurrently() {
        val db = PointsDatabase()
        val ms = measureTimeMillis {
            runBlocking {
                listOf(
                        db.addPointsAsync("Hans", 1000),
                        db.addPointsAsync("Hans", 800),
                        db.addPointsAsync("Hans", 400)
                ).map { it.await() }
            }
        }
        println("Took $ms ms")
        assert(db.data["Hans"]?.points).isEqualTo(2200)
    }

    @Test
    fun incrementPointsConcurrentlyOnSingleThread() {
        val db = PointsDatabase()
        val ms = measureTimeMillis {
            runBlocking {
                listOf(
                        db.addPointsAsyncSingleThread("Hans", 1000),
                        db.addPointsAsyncSingleThread("Hans", 800),
                        db.addPointsAsyncSingleThread("Hans", 400)
                ).map { it.await() }
            }
        }
        println("Took $ms ms")
        assert(db.data["Hans"]?.points).isEqualTo(2200)
    }

    @Test
    fun incrementPointsMassivelyConcurrent() {
        val db = PointsDatabase()
        val ms = measureTimeMillis {
            runBlocking {
                (1..1_000).map {
                    db.addPointsAsyncSingleThread("Hans", 1_000)
                }.map { it.await() }
            }
        }
        println("Took $ms ms")
        assert(db.data["Hans"]?.points).isEqualTo(1_000_000)
    }

    @Test
    fun incrementPointsMassivelyThread() {
        val db = PointsDatabase()
        val singleThreadExecutor = Executors.newFixedThreadPool(1)
        val ms = measureTimeMillis {
            (1..1_000).map {
                thread(start = true) {
                    println("Started $it")
                    for (i in 1..1_000) {
                        sleep(1)
                        singleThreadExecutor.execute {
                            db.addPoint("Hans")
                        }
                    }
                    println("Done with $it")
                }
            }.map { it.join() }
        }
        println("Took $ms ms")
        assert(db.data["Hans"]?.points).isEqualTo(1_000_000)
    }

}
