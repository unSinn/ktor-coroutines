# Testing Kotlin Coroutines with Ktor

HTTP GET Points

    watch -n 0.5 http get localhost:8080/points

HTTP PUT long running blocking job

    http put localhost:8080/addblocking name=hannes points=10000
    
HTTP PUT long running async concurrent job

    http put localhost:8080/addasync name=peter points=10000
    http put localhost:8080/addasync name=peter points=10000

HTTP PUT long running async concurrent job synchronized on a single thread

    http put localhost:8080/addasyncsingle name=heinz points=10000
    http put localhost:8080/addasyncsingle name=heinz points=10000
    
Observe the different results